import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:film/src/entities/movie.dart';
import 'mapper/movie_mapper.dart';

class MovieApi {
  Future<List<Movie>> getMovies() async {
    final url = Uri.parse(
        'https://api.kinopoisk.cloud/movies/all/page/666/token/8080e23ae8d77310e981b929e52dbe0a');
    http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      return MoviesMapper.fromMap(json.decode(response.body));
    } else {
      throw Exception('Error: ${response.reasonPhrase}');
    }
  }
}

// https://api.kinopoisk.cloud/movies/all/page/666/token/8080e23ae8d77310e981b929e52dbe0a
// https://api.kinopoisk.cloud/movies/all/page/666/token/0fcc2cb252a467f375c646f867fa9059
// https://api.kinopoisk.cloud/movies/all/page/666/token/9b9884ccd7c804699ace49723bc62a67
