import 'package:film/src/entities/movie.dart';

extension MovieMapper on Movie {
  static Movie fromMap(Map map) {
    return Movie(
        title: map['title'] as String,
        posterUrl: 'https:' + map['poster'],
        description: map['description'] as String);
  }
}

extension MoviesMapper on List<Movie> {
  static List<Movie> fromMap(Map map) {
    return (map['movies'] as List).map((e) => MovieMapper.fromMap(e)).toList();
  }
}
