import 'package:shared_preferences/shared_preferences.dart';

const favoriteStoreKey = 'favorite_store';

class FavoriteStore {
  Future<List<String>> getNames() async {
    final preferences = await SharedPreferences.getInstance();
    return preferences.getStringList(favoriteStoreKey) ?? [];
  }

  Future<void> saveName(String name) async {
    final names = await getNames();
    names.add(name);
    final preferences = await SharedPreferences.getInstance();
    return preferences.setStringList(favoriteStoreKey, names);
  }

  Future<void> removeName(String name) async {
    final names = await getNames();
    names.remove(name);
    final preferences = await SharedPreferences.getInstance();
    return preferences.setStringList(favoriteStoreKey, names);
  }
}
