import 'package:flutter/material.dart';
import 'package:film/src/entities/movie.dart';
import 'package:film/src/api/src/movie_api.dart';
import 'package:film/src/pages/movie_page.dart';

import 'favorite_page.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final movieApi = MovieApi();
  List<Movie> loadedMovies;
  String filter = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('фильмы'),
      ),
      body: SafeArea(
        child: FutureBuilder<List<Movie>>(
          future: movieApi.getMovies(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: errorWidget(),
              );
            }
            if (snapshot.hasData) {
              loadedMovies = snapshot.data;
              return Column(
                children: [
                  searchWidget(),
                  ElevatedButton(
                    child: Text(
                      'Избранное',
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => FavoritePage(loadedMovies),
                      ),
                    ),
                  ),
                  Expanded(
                      child: moviesWidget(
                          movies: filterMovies(
                    movies: loadedMovies,
                    filter: filter,
                  )))
                ],
              );
            }
            return Center(
              child: loadingWidget(),
            );
          },
        ),
      ),
    );
  }

  List<Movie> filterMovies({
    @required List<Movie> movies,
    @required String filter,
  }) {
    return movies.where((e) => e.title.toLowerCase().contains(filter)).toList();
  }

  Widget searchWidget() {
    return Column(
      children: [
        TextField(
          onChanged: (value) {
            filter = value;
          },
          decoration: InputDecoration(
            hintText: 'название фильма',
            fillColor: Colors.blue[100],
            filled: true,
          ),
        ),
        ElevatedButton(
          child: Text(
            'поиск',
            style: TextStyle(fontSize: 20),
          ),
          onPressed: () => setState(() {}),
        )
      ],
    );
  }

  Widget moviesWidget({@required List<Movie> movies}) {
    return ListView.builder(
      itemCount: movies.length,
      itemBuilder: (context, index) {
        return Card(
          child: ListTile(
            title: Text(movies[index].title),
            leading: Image.network(movies[index].posterUrl),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return MoviePage(movie: movies[index]);
                  },
                ),
              );
            },
          ),
        );
      },
    );
  }

  Widget loadingWidget() {
    return CircularProgressIndicator();
  }

  Widget errorWidget() {
    return Text('ошибка');
  }
}
