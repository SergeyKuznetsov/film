import 'package:film/src/store/favorite_store.dart';
import 'package:flutter/material.dart';
import 'package:film/src/entities/movie.dart';
import 'package:image_downloader/image_downloader.dart';

class MoviePage extends StatefulWidget {
  MoviePage({@required this.movie});

  final Movie movie;

  @override
  _MoviePageState createState() => _MoviePageState();
}

class _MoviePageState extends State<MoviePage> {
  final favoriteStore = FavoriteStore();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.movie.title),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Center(
          child: FutureBuilder<List<String>>(
            future: favoriteStore.getNames(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                final isFavorite = snapshot.data.contains(widget.movie.title);
                return Column(
                  children: [
                    if (!isFavorite)
                      ElevatedButton(
                        child: Text('добавить в избранное'),
                        onPressed: () async {
                          await favoriteStore.saveName(widget.movie.title);
                          setState(() {});
                        },
                      ),
                    if (isFavorite)
                      ElevatedButton(
                        child: Text('удалить из избранного'),
                        onPressed: () async {
                          await favoriteStore.removeName(widget.movie.title);
                          setState(() {});
                        },
                      ),
                    ElevatedButton(
                      onPressed: () async {
                        try {
                          await ImageDownloader.downloadImage(
                              widget.movie.posterUrl);
                        } catch (e) {
                          print(e);
                        }
                      },
                      child: Text('Сохранить превью'),
                    ),
                    Expanded(
                      child: Image.network(widget.movie.posterUrl),
                    ),
                    Expanded(
                      child: Text(
                        widget.movie.title,
                        style: TextStyle(fontSize: 30),
                      ),
                    ),
                    Expanded(
                      child: Text(
                        widget.movie.description,
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                );
              }
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}
