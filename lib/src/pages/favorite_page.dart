import 'package:film/src/entities/movie.dart';
import 'package:film/src/store/favorite_store.dart';

import 'package:flutter/material.dart';

import 'movie_page.dart';

class FavoritePage extends StatefulWidget {
  FavoritePage(this.movies);

  final List<Movie> movies;

  @override
  FavoritePageState createState() => FavoritePageState();
}

class FavoritePageState extends State<FavoritePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Избранное')),
      body: SafeArea(
        child: FutureBuilder<List<String>>(
          future: FavoriteStore().getNames(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final moviesNames = snapshot.data;
              return moviesWidget(
                movies: widget.movies
                    .where(
                      (e) => moviesNames.contains(e.title),
                    )
                    .toList(),
              );
            }
            return Center(
              child: loadingWidget(),
            );
          },
        ),
      ),
    );
  }

  Widget loadingWidget() {
    return CircularProgressIndicator();
  }

  Widget moviesWidget({@required List<Movie> movies}) {
    return ListView.builder(
      itemCount: movies.length,
      itemBuilder: (context, index) {
        return Card(
          child: ListTile(
            title: Text(movies[index].title),
            leading: Image.network(movies[index].posterUrl),
            onTap: () async {
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return MoviePage(movie: movies[index]);
                  },
                ),
              );
              setState(() {});
            },
          ),
        );
      },
    );
  }
}
