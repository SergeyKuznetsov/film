import 'package:meta/meta.dart';

class Movie {
  Movie({
    @required this.title,
    @required this.posterUrl,
    @required this.description,
  });

  final String title;
  final String posterUrl;
  final String description;
}
